﻿/**
 *  @CameraFollow.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script will make the Camera GameObject follow a target GameObject.
 */

using UnityEngine;

public class CameraFollow : MonoBehaviour {

    //A public variable that holds a game object, this will later be used for the monster game objects transform
    public GameObject target;

    //A private variable that holds the game objects position later on for the camera to follow
    private Transform targetPos;


    // Use this for initialization
    void Start() {

        //The targetPos variable is equal to the target transform (Which is the monsters tranform)
        targetPos = target.transform;
    }

    // Update is called once per frame
    void LateUpdate() {

        //The transform position of the monster's postion is equal to a new vector three that keeps track of it's position constantly
        if (targetPos) {
            transform.position = new Vector3(targetPos.position.x, targetPos.position.y, transform.position.z);
        }
    }

}

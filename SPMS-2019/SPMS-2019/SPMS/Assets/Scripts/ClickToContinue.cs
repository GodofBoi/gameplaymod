﻿/**
 *  @ClickToContinue.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script will load a new Scene when the user clicks the mouse.
 */

using UnityEngine;
using UnityEngine.SceneManagement;

//Public class so the functions within the click to continue 
public class ClickToContinue : MonoBehaviour {

    //Public string variable created called scene, it will contain an empty slot in the inspector that can hold a untiy scene to load instead
    //of manually calling it through code
	public string scene;

    //A private bool variabale called loadLock that's equal to false right away
	private bool loadLock = false;
	
	void Update () {

        //If the input of the left button of the mouse is pressed down and loadLock is not equal to loadLock, it will load the scene in the
        //Inspector
		if(Input.GetMouseButtonDown(0) && !loadLock)
		{
            //Calls the load scene function after the mouse is pressed.
			LoadScene();
		}
	}

    //A load scene funciton that loads the scene if the scene is equal to null (Meaning it's not equal to anything)
	void LoadScene()
	{
		if (scene == null)
			return;

        //If the scene is equal to null, loadLock is equal to true and the scenemanger loads the scene that was placed in the inspector.
		loadLock = true; 
        SceneManager.LoadScene(scene);
	}
}

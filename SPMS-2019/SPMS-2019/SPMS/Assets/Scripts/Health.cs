﻿/**
 *  @Health.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script will manage the health of an GameObject and alows you to
 *  take damage as well as display another GameObject when killed.
 */
using UnityEngine;
using System.Collections;

// A public class called Health meaning other scripts can call from this one
public class Health : MonoBehaviour
{
        // A public int variable that dictates the max health the player has, it is equal to ten
		public int maxHealth = 10;

        //A public int variable that controls the amount of health the player has in the moment
		public int health = 10;

        //A public game object variable called deathInstance which is set to null
		public GameObject deathInstance = null;

        //A public vector 2 that controls the distance from which the dead body of the monseter is from the player's original position
		public Vector2 deathInstanceOffset = new Vector2 (0, 0);

		void Start ()
		{
                //The health variable is equal to the max health variable on start up which is 10
				health = maxHealth;
		}
        
        // A public void funciton called take damage which holds a int varaible
		public void TakeDamage (int value)
		{
                //A way to show the developer in console how much and at what time the player is taking damage
                //The take damage funcition is combined with the value of how much the damage is worth
				Debug.Log ("Take Damage " + value);

                //The health variable is equal to inself minus the value of damage that is being taken
				health -= value;
		    
                //If the health variable reaches zero or is below it, the player will be killed
				if (health <= 0) {

                        //Calls the onKill funciton 
						OnKill ();
				}
		}
	
        //A public function that controls the player's death sprite
		public void OnKill ()
		{
                //If the player is equal to deathInstance the following will happen...
				if (deathInstance) {

                        //The player's vector 3 position will be changed, the game object will be moved using the transform
						Vector3 position = gameObject.transform.position;
                        
                        //A new sprite will take place of the old sprite, which is the death sprite
						Instantiate (deathInstance, new Vector3 (position.x + deathInstanceOffset.x, position.y + deathInstanceOffset.y, position.z), Quaternion.identity);
				}

                //Destroy the original game object that is the player
				Destroy (gameObject);
		}

}

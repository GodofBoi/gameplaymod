﻿/**
 *  @ControlsUI.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script Visualizes the health value on a GameObject with the
 *  Health script attached to it.
 */
using UnityEngine;
using UnityEngine.UI;

// Public class health bar
public class HealthBar : MonoBehaviour {

    // Calling the health component from the health script and creating a variable called healthComponent to refer to it
    Health healthComponent;

    // Using the image function from the inspector, the code uses the middle health image which is the actual health bar.
    public Image healthImage;


    void Awake() {

        //Using the Health class component from the other script 
        healthComponent = GetComponent<Health>();
    }
    void Start() {

        //Transforms the image of the middle health bar by a newly stated vector 3
        healthImage.rectTransform.localScale = new Vector3(1, 1, 1);
    }

    void Update() {

        //A new float variable is declared called percent, that percent variable is equal to the health component and 
        float percent = healthComponent.health / (float)healthComponent.maxHealth;

        //The health image x value is being transformed by a percent, mimicing a health bar decreasing 
        healthImage.rectTransform.localScale = new Vector3(percent, 1, 1);
    }

}

﻿/**
 *  @Attack.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script will allow a GameObject to attack another GameObject if it
 *  has a reference to the Health Script.
 */

using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour {

    // A public variable int that will allow the player to attack
	public int attackValue = 1;

    //A pulbic float variable that provides a delay for the attack by 1
	public float attackDelay = 1f;

    //Looks for the target tag which is knights
	public string targetTag;

    //A bool that decides whether or not the monster will attac
	private bool canAttack;

	// Use this for initialization
	void Start () {
		if (attackValue <= 0)
			canAttack = false;
		else
			StartCoroutine(OnAttack());

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionStay2D(Collision2D c)
	{
        //If hte collision is with the target tags abd can attack is true, the player attacks the game object
		if (c.gameObject.tag == targetTag)
		{
			if (canAttack)
				TestAttack(c.gameObject);
		}
	}

	void TestAttack(GameObject target)
	{
        //Both statements check to see if the players position is able to attack the knights from left and right sides.
		if (transform.localScale.x == 1)
		{
			if (target.transform.position.x > transform.position.x)
				AttackTarget(target);
		}
		else
		{
			if (target.transform.position.x < transform.position.x)
				AttackTarget(target);
		}
        //If not, the player can not attack
		canAttack = false;
	}

	void AttackTarget(GameObject target)
	{
        //Grabbing the health component from the health script
        Health healthComponent = target.GetComponent<Health>();

        //If the healthcomponent is equal to true, the player takes damage by the attack value
		if (healthComponent)
			healthComponent.TakeDamage(attackValue);
	}

	IEnumerator OnAttack()
	{
        //Creates the time window from where the attack variable for the player is set to true for one second.
		yield return new WaitForSeconds(attackDelay);
		canAttack = true;
		StartCoroutine(OnAttack());
        //This repeats everytime the player attacks
	}

}

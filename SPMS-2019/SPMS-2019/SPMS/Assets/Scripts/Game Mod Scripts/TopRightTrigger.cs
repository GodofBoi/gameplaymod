﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopRightTrigger : MonoBehaviour
{
    //Grabbing the collider component from the inspector and calling the variable KillZone
    Collider2D KillZone;

    //Sprite renderer component from the inspector to use for showing the player the killzone is on
    SpriteRenderer Arrow;

    // Start is called before the first frame update
    void Start()
    {
        KillZone = GetComponent<Collider2D>();
        Arrow = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //Enables the kill zone so the knights can die
        if (Input.GetButtonDown("E"))
        {
            KillZone.enabled = !KillZone.enabled;
            Arrow.enabled = !Arrow.enabled;
        }

    }
}

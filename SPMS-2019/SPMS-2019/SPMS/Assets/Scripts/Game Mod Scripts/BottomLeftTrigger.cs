﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomLeftTrigger : MonoBehaviour
{
    //Grabbing the collider component from the inspector and calling the variable KillZone
    Collider2D KillZone;

    //Sprite renderer component from the inspector to use for showing the player the killzone is on
    SpriteRenderer Arrow;

    // Start is called before the first frame update
    void Start()
    {
        KillZone = GetComponent<Collider2D>();
        Arrow = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //Enables the kill zone so the knights can die
        if (Input.GetButtonDown("S"))
        {
            KillZone.enabled = !KillZone.enabled;
            //Also activates the arrow sprite to show where the hitbox is
            Arrow.enabled = !Arrow.enabled;
        }

    }
}

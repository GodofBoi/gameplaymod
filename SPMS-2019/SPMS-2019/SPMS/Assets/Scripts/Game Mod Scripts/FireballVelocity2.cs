﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballVelocity2 : MonoBehaviour
{
  
    //Calculates the speed at which the fireball goes at
    public float speed = 5;

    //Grabs the rigidboyd2d componenet from the inspector
    Rigidbody2D rigidbody2dcomponet;

    // Start is called before the first frame update
    void Start()
    {
        //Makes use of the rigidbody2d component in script
        rigidbody2dcomponet = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Sets the velocity of the rigidbody to a new vector 2 that has a 2 in x value multiplied by speed value
        rigidbody2dcomponet.velocity = new Vector2(-2, 0) * speed;

    }

    void Charging(GameObject KillCount)
    {
        KnightsKilledScore KnightScoreComponent = KillCount.GetComponent<KnightsKilledScore>();


        KnightScoreComponent.KnightPreFix = KnightScoreComponent.KnightPreFix + 1;
    }
}


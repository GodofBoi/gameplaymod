﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KnightsKilledScore : MonoBehaviour
{
    //Knight kill count variables to keep track of score
    public static int Knights;
    public string KnightPreFix;
    public Text KnightKillCount;

    //A private string format that controls the text input
    string stringformat = "#";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        //Calls the UpdateHud funciton
        UpdateHud();
    }

    void UpdateHud()
    {
        KnightKillCount.text = KnightPreFix + Knights.ToString(stringformat);
    }
}

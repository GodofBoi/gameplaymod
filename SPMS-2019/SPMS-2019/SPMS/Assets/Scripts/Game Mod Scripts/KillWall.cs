﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillWall : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //If a collision happens with the trigger and the tag is equal to KillWall destroy the knight game object
        if(collision.gameObject.tag == "KillWall")
        {
            Destroy(gameObject);
            
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillKnight : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collision)
    {
        //If the knights run into the fireball they die and the knights killed score gets increased by 1
        if (collision.gameObject.tag == "Fire")
        {
            Destroy(gameObject);
            KnightsKilledScore.Knights++;
        }

        //If a knight runs into a knight kill block they will die and the knights killed score and the fireball charges gets increased by 1
        if (collision.gameObject.tag == "KnightKillBlock")
        {
            Destroy(gameObject);
            KnightsKilledScore.Knights++;
            Fireball.Charges++;
        }
    }
}

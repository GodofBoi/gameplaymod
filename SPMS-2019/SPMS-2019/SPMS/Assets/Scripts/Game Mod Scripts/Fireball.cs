﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fireball : MonoBehaviour
{
    //Keeps track of the first fire ball to use later
    public GameObject FireballProjectile;

    //Keeps track of the second form of the fire ball for later instantiation
    public GameObject FireballProjectile2;

    public GameObject Knights;

    //Charge count to keep track of charge score
    public static int Charges;
    public string ChargesPreFix;
    public Text ChargeCount;

    string stringsformat = "#";

    //A float variable to keep track of the max charges of the fireball meter
    bool chargeMax = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //If R or Q are pressed they will fire a fireball in a certain direction dependent on button
        if (Input.GetButtonDown("R"))
        {
            if (chargeMax == true)
            {
                Instantiate(FireballProjectile, new Vector2(0, 0), Quaternion.identity);
            }
        }
        if (Input.GetButtonDown("Q"))
        {
            if (chargeMax == true)
            {
                Instantiate(FireballProjectile2, new Vector2(0, 0), Quaternion.identity);
            }
        }

        //Calls the update hud funciton to keep the charges for fireball counted
        UpdateHud();
    }

    void UpdateHud()
    {
        //A simple counter for keeping track how many charges the player has.
        ChargeCount.text = ChargesPreFix + Charges.ToString(stringsformat);

        //If the charges is at the number 4 or greater it will revert back to 4
        if(Charges >= 4)
        {
            //If the charges are equal to four charge max is equal to true which allows the player to fire a fireball
            chargeMax = true;
            Charges = 4;
        }
    }
}

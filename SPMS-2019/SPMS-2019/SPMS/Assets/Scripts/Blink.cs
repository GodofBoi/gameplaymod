﻿/**
 *  @Blink.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script will hide and show a GameObject based on a set time dealy.
 */
using UnityEngine;
using System.Collections;

// The class blink is made public so it can be called in other scripts
public class Blink : MonoBehaviour {

    //A public float value called delay that later controls the speed at which the text flashes from white to black
    public float delay = .5f;

    //Calls the sprite renderer component from the unity inspector 
    SpriteRenderer spriteRendererComponent;

    void Awake() {
        //Allows the use of the spriterenderer, the component is called "spriteRendererComponent"
        spriteRendererComponent = GetComponent<SpriteRenderer>();
    }
    // Use this for initialization
    void Start() {

        //Starts the coroutine in the OnBlink function
        StartCoroutine(OnBlink());
    }

    IEnumerator OnBlink() {

        //The yield waits for the coroutine to finish
        //The value of the colour of the sprite being used repeats here with a delay of a previously declared float called delay
        yield return new WaitForSeconds(delay);

        //A new float variable is declared called alpha, it's equal to the sprites colour alpha
        float alpha = spriteRendererComponent.color.a;

        // A new float variable is declared called newAlpha, it is equal to zero and later is the cause of the sprites changing colours
        float newAlpha = 0f;

        //If the alpha variable does not equal 1, newAlpha variable is equal to 1 which changes the sprite colour
        if (alpha != 1) {
            newAlpha = 1f;
        }

        //The sprite's colour inside the game is equal to a new colour value with an added aplha value at the end
        spriteRendererComponent.color = new Color(1, 1, 1, newAlpha);

        //At the end of the on blick function the start coroutine will begin again over and over
        StartCoroutine(OnBlink());
    }
}

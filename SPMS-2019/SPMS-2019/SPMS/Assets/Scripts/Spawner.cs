﻿/**
 *  @Spawner.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script will spawn new GameObjects to its own location or to a
 *  set of targets.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour {

    //Creates an array of gameobjets called enemy pool that's used to spawn the enemies
	public GameObject[] enemyPool;

    //A float that creates a delay for the spawner so enemies are not constantly spawning
	public float delay = 2.0f;

    //A bool that sets the spawns to active or unactive
	public bool isActive = true;

    //A vector 2 variable that controls direction
	public Vector2 direction = new Vector2(1, 1);

    //A list of gameobjects that's private with the name targets
	private List<GameObject> targets;

    //A private gizmo that has the name parent gizmo, grabs the component of the gizmo from the inspector
	private Gizmo parentGizmo;

	void Start () {
		parentGizmo = gameObject.GetComponent<Gizmo>();
		targets = parentGizmo.targets;
		StartCoroutine(EnemyGenerator());
	}

    //Beginning of the spawn enemy genreation
	IEnumerator EnemyGenerator()
	{
        //If is active is true
		if (isActive)
		{
            //A new transform that adjusts the enmeies postions when they spawn
			Transform newTransform = transform;
			
            //Returns the seconds that's created by the delay when reset and repeats
			yield return new WaitForSeconds(delay);
			
           
			if (targets.Count > 0)
			{
				GameObject spawnTarget  = targets[Random.Range(0, targets.Count)];
				newTransform = spawnTarget.transform;
				direction = spawnTarget.transform.localScale;
			}
			
            //Spawns a clone of a knight and instantiate it in a pool list of enemies that has an adjustable range
			GameObject clone = Instantiate(enemyPool[Random.Range(0, enemyPool.Length)], newTransform.position, Quaternion.identity) as GameObject;
            //This also directs the enemies spawned which direction they should go in
			clone.transform.localScale = direction;
            //Reset the coroutine
			StartCoroutine(EnemyGenerator());
		}
	}

}

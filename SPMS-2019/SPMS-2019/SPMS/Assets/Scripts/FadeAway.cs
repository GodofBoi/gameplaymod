﻿/**
 *  @FadeAway.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script will fade a GameObject to 0 alpha and destroy it automatically.
 */

using UnityEngine;
using System.Collections;

// A publically decalred class called FadeAway, this means other scripts can call from this one
public class FadeAway : MonoBehaviour {

    //A public bool variable called autoDistory that's set to true
    public bool autoDistroy = true;

    //A public float variable called delay that's equal to 2, this delay controls the speed at which the knights disapear after being killed
    public float delay = 2.0f;

    //Makes the sprite renderer component from the Unity inspector usable in code
    SpriteRenderer spriteRendererComponent;

    void Awake() {

        //Calls the sprite renderer component 
        spriteRendererComponent = GetComponent<SpriteRenderer>();
    }
    // Use this for initialization
    void Start() {

        //Starts the coroutine 
        StartCoroutine(FadeTo(0.0f, 1.0f));
    }

    IEnumerator FadeTo(float aValue, float aTime) {
        yield return new WaitForSeconds(delay);

        float alpha = transform.GetComponent<Renderer>().material.color.a;
        for (float t = 0.0f; t <= 1.0f; t += Time.deltaTime / aTime) {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
            spriteRendererComponent.color = newColor;

            if (newColor.a <= 0.05) {
                Destroy(gameObject);
            }
            yield return null;
        }

    }
}

﻿/**
 *  @Player.cs
 *  @version: 1.00
 *  @author: Jesse Freeman
 *  @date: Feb 3
 *  @copyright (c) 2012 Jesse Freeman, under The MIT License (see LICENSE)
 * 
 * 	-- For use with Weekend Code Project: Unity's New 2D Workflow book --
 *
 *  This script will allow you to move a GameObject, idealy the player,
 *  via the keyboard and the mouse. This will slide GameObject forward
 *  based on the direction of the input.
 */

using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    //A public float variable called speed
    public float speed = 200;

    //A public float variable that keeps track of the maxinum speed
    public float maxSpeed = 5;

    //A private int variable called moving that starts at 0
    int moving = 0;

    //A private float that will later keep track of the mouse x position
    float mouseX = 0;

    //Declaring a rigidbody 2d component from unity inspector
    Rigidbody2D rigidbody2DComponent;

    void Awake() {

        // Gets the rigidbody 2d component
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {

        //If the left mouse button is pressed the mouse x postion will be tracked between the two halfs of the screen
        if (Input.GetMouseButtonDown(0)) {
            mouseX = 90 * ((Input.mousePosition.x - Screen.width / 2) / (Screen.width / 2));
        } else {
            //The mouse button will not trigger teh mouseX variable and won't move the character
            mouseX = 0;
        }

        //If the input is the right key or the mouseX is greater then 0 the monster will move right by 1
        if (Input.GetKey("right") || mouseX > 0) {
            moving = 1;

            //If the input is left or the mouseX is less then 0 the monster will move left by -1
        } else if (Input.GetKey("left") || mouseX < 0) {
            moving = -1;
        } else {

            //The monster doesn't move
            moving = 0;
        }

        //If the moving variable does not equal 0
        if (moving != 0) {

            //A new float is created called velocity X which is equal to the rigidbody 2d components velocity for x
            float velocityX = System.Math.Abs(rigidbody2DComponent.velocity.x);

            //If the velocityX is less then .5 then add a force to the rigidbody 2d component
            if (velocityX < .5) {
                rigidbody2DComponent.AddForce(new Vector2(moving, 0) * speed);


                if (this.transform.localScale.x != moving)
                    this.transform.localScale = new Vector3(moving, 1, 1);
            }

            //If velocityX is greater then max speed it will cap off at max speed
            if (velocityX > maxSpeed)
                rigidbody2DComponent.velocity = new Vector2(maxSpeed * moving, 0);
        }

    }
}
